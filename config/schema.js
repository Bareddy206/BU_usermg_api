const table = require('./table');
const dynogels = require('./database').dynogels;
const Bluebird = require('bluebird');

module.exports = function (Schema, optionsObj) {
	const tableName = optionsObj.tableName || table.tableName;
	const Model = dynogels.define(tableName, Schema);
	const selectTable = (name)=>{
		Model.config({tableName: name});
	};

	// //CRUD operations for Models
	// we can pass an array of items and they will all be created
	const createItem = (params, optionsObj) => {
	    return new Bluebird((resolve, reject) =>{
			const localTable = optionsObj.table || tableName;
			const createCallback = ()=> {
				selectTable(localTable);
				Model.create(params, {overwrite:optionsObj.overwrite || false}, 
					(err, createdData) => { //{overwrite:false} to not overwrite existing records
					if (err){
						console.log('\nItem creation err\n');
						reject(err);
					}
					else {
						console.log('\nCreated an item in the table\n', );
						resolve(createdData.attrs);
					}
				});
			}
			table.checkAndCreateTable(localTable, createCallback);
	   });
	};

	const getItem = (params, optionsObj) => {
	
		return new Bluebird((resolve, reject) =>{

			const getCallback = (err, result)=> {
				if (err) {
					console.log(`Get item err..\n\n`, err);
					reject(err);
				} else if (!result) {
					console.log(`\nNo item found..`);
					resolve({});
				} else {
					console.log(`\nGet item success..`);
					resolve(result.attrs)
				}
			}

			if (!optionsObj || !optionsObj.attToGet) {
				Model.get(params, getCallback);
			}
			else {
				Model.get(params, {
					AttributesToGet: optionsObj.attToGet
				}, getCallback);
			}
	   })

	};

	const updateItem = (params, conditionObj)=> {
	    return new Bluebird((resolve, reject) =>{

			Model.update(params, conditionObj,(err, updatedData) => {
				if (err){
					console.log('\nItem Update error', err);
					reject(err);
				} 
				else {
					console.log('\nUpdated data');
					resolve(updatedData.attrs);
				}
			});
	    });
	};

	const deleteItem = (params, conditionObj) => {
		return new Bluebird((resolve, reject) =>{
			//$del will remove items from the object
			Model.destroy(params, conditionObj, (err,data) => {
				if (err){
					console.log('\nItem Delete error', err);
					reject(err);
				} 
				else {
					console.log(`\nRemoved an object with ${params.collectionId} from db`);
					resolve(data.attrs);
				}
			})
	    })
	};

	const batchGetItem = (params, conditionObj) => {
		return new Bluebird((resolve, reject) =>{

			Model.getItems(params, conditionObj, (err, batchData) => {
				if (err){
					console.log('\nbatchGetItem error', err);
					reject(err);
				} 
				else {
					console.log(`\nBatch items:`);
					resolve(batchData.attrs);
				}
			})
	    })
	};

	const query = (hashkey) => {

		return new Bluebird((resolve, reject) =>{
		
			const queryCallback = (err, data)=> {
				if (err) {
					console.log('Query error:\n\n', err);
					reject(err);
				}
				else {
					console.log(`\nQuery Batch items:`);
					const dataAttrs = data.Items.map((value)=> value.attrs);
					resolve(dataAttrs);
				}
			}

			if (!optionsObj)
				Model.query(hashkey).loadAll().exec(queryCallback);
			else
				Model.query(hashkey).attributes(optionsObj.attToQuery).exec(queryCallback);
		})

	};

	return {
		selectTable,
		createItem,
		getItem,
		updateItem,
		deleteItem,
		batchGetItem,
		query
	}

}