// Mailgun mail content
const mailTestLink = 'https://www.w3schools.com/test/names.asp';
const fromMail = 'dev@predator-digital.com';

const sendVerificationMail = (userMail, storeName ,verification_code,rootUrl,pwd) => {
	const tempUrl = `${rootUrl || mailTestLink}/api/users/signupverification`;
	return {
		from: `Cadenza <${fromMail}>`,
		to: `${userMail}`,
		subject: 'Verification mail',
		html:`<!doctype html>
		<html lang="en">
		   <head>
			  <meta charset="utf-8" />
			  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
			  <!-- <link rel="apple-touch-icon" sizes="76x76" href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/apple-icon.png" /> -->
			  <link rel="icon" type="image/png" href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/favicon.png" />
			  <title>Verify</title>
			  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
			  <meta name="viewport" content="width=device-width" />
			  <!-- CSS Files -->
			  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			  <link href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/css/styles.css" rel="stylesheet" />
			  <!-- Fonts and Icons -->
			   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> 
		
			  <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
		
		
		   </head>
		   <body style="color: #66615b;
		   font-size: 14px;
		   font-family: 'Muli', Arial, sans-serif;">
			  <div class="image-container set-full-height" style=" min-height: 100vh;
			  background-position: center center;
			  background-size: cover;
			  background-image: url('https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/bg.png');">
				 <div class="header" style="float:left; width: 100%;">
					<div class="logosection" style="width: 14%;margin: 40px 36% 0px;">         
					   <a href="#"><img src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/new_logo.png"></a>
					</div>
				 </div>
				 <!--   Big container   -->
				 <div class="container" style="margin-right:auto;margin-left:auto;padding-left:15px;padding-right:15px;box-sizing: border-box;">
		
					<div class="row" style="margin-left:-15px;margin-right:-15px">
					   <div class="col-sm-4 col-sm-offset-4" style="margin-left:33.33333333%;width:33.33333333%;">
						  <!--      WizardTables container        -->
						  <div class="wizard-container" style="z-index: 3;" >
						  <div class="wizard-header1">
						  <div class="signuphead" style=" float: right; 
						  width: 25%;
						  background: #2e3442;
						  color: #fff;
						  text-align: center;
						  padding: 5px 0%;
						  border: 1px solid #2e3442;
						  border-radius: 10px 10px 0px 0px;
						  font-size: 13px;
						  font-weight: bolder;">Verify</div>
					   </div>
							 <div class="signupbody" style="float: left; width: 100%; border-top: 5px solid #2e3442; border-radius: 10px 0px 0px 0px;">
								<div class="card wizard-card" data-color="green" style=" min-height: 350px;
								box-shadow: 0 20px 16px -15px rgba(0, 0, 0, 0.57);  border-radius: 6px;
								box-shadow: 0 2px 2px rgba(204, 197, 185, 0.5);
								background-color: #fafafa;
								color: #252422;
								padding: 10px 0;
								margin-bottom: 20px;
								position: relative;
								z-index: 1;" >
								   <form action="">
									  <div class="row" style="margin-left:-15px;margin-right:-15px">
										 <div class="col-sm-10 col-sm-offset-1"style="width:83.33333333%;margin-left:8.33333333%">
											<div class="col-sm-12" style="float:left; width:100%">
											   <div class="wizard-header1">
												  <div class="usericon" style="width: 15%;
												  margin: auto;"><img alt="verify" src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/verify.png"></div>
											   </div>
											   <div class="row" style="margin-left:-15px;margin-right:-15px">
												  <div class="col-sm-12" style="float:left; width:100%">
													 <div class="verifyinfo" style=" float: left;
													 width: 100%;
													 text-align: center;
													 padding: 20px 0%;">Thanks for choosing our service</div>
												  </div>
											   </div>
											   <div class="row" style="margin-left:-15px;margin-right:-15px">
												  <div class="col-sm-12" style="float:left; width:100%">
													 <div class="verifyinfo" style=" float: left;
													 width: 100%;
													 text-align: center;
													 padding: 20px 0%;">To continue to login</div>
												  </div>
											   </div>
											   <div class="row" style="margin-left:-15px;margin-right:-15px">
												  <div class="col-sm-12" style="float:left; width:100%">
													 <div class="verifyinfo" style=" float: left;
													 width: 100%;
													 text-align: center;
													 padding: 20px 0%;">Click to verify your email id</div>
											   </div>
											   <div class="row" style="margin-left:-15px;margin-right:-15px">
												  <a href="${tempUrl}?email=${userMail}&verification_code=${verification_code}&storeName=${storeName}&&pwd=${pwd}"><input type="submit" name="submit" class="submitlink" style="float: left;
												  background-color: #2e3442;
												  border: #2e3442;
												  border-radius: 5px;
												  width: 50%;
												  text-align: center;
												  padding: 8px 0%;
												  color: #FFFFFF;
												  font-weight: bold;
												  margin: 30px 25%;" value="Verify"></a>
											   </div>
											</div>
										 </div>
									  </div>
								   </form>
								</div>
							 </div>
						  </div>
						  <!-- wizard container -->
					   </div>
					</div>
					<!-- row -->
				 </div>
				 <!--  big container -->
			  </div>
		   </body>
		   <!--   Core JS Files   -->
		   <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>  
		   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		   <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
		   <!--  Plugin for the Wizard -->
		   <script src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/js/paper-bootstrap-wizard.js" type="text/javascript"></script>  
		   <script src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/js/jquery.validate.min.js" type="text/javascript"></script>
		   <script>
			  $(document).ready(function(){            
				  //$("#basketone").show();
				  $("#baskettwo").show();
			  
				  //$("#planone").show();
				  $("#plantwo").show();    
			  
				  //$("#paymentone").show();
				  $("#paymenttwo").show(); 
			  });
		   </script>   
		</html>`
	};
};

const forgotPasswordMail = (userMail,rootUrl,temp) => {
	return {
		from: `Store owner <${fromMail}>`,
		to: `${userMail}`,
		subject: 'Forgot password',
		//html: `<p>Please use this link to reset your password
		//${rootUrl}/resetpassword?email=${userMail}&code=${temp}</p>`
		html:`<!doctype html>
        <html lang="en">
           <head>
              <meta charset="utf-8" />
              <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
              <!-- <link rel="apple-touch-icon" sizes="76x76" href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/apple-icon.png" /> -->
              <link rel="icon" type="image/png" href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/favicon.png" />
              <title>Verify</title>
              <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
              <meta name="viewport" content="width=device-width" />
              <!-- CSS Files -->
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
              <link href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/css/styles.css" rel="stylesheet" />
              <!-- Fonts and Icons -->
               <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> 
        
               <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu" />
        
        
           </head>
           <body style="color: #66615b;
           font-size: 24px;
           font-family: 'Ubuntu">
              <div class="image-container set-full-height" style=" min-height: 100vh;
              background-position: center center;
              background-size: cover;
              background-image: url('https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/bg.png');">
                 <div class="header" style="float:left; width: 100%;">
                    <div class="logosection" style="width: 14%;margin: 40px 36% 0px;">          
                       <a href="#"><img src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/new_logo.png"></a>
                    </div>
                 </div>
                 <!--   Big container   -->
                 <div class="container" style="margin-right:auto;margin-left:auto;padding-left:15px;padding-right:15px;box-sizing: border-box;">
        
                    <div class="row" style="margin-left:-15px;margin-right:-15px">
                       <div class="col-sm-4 col-sm-offset-4" style="margin-left:33.33333333%;width:33.33333333%;">
                          <!--      WizardTables container        -->
                          <div class="wizard-container" style="z-index: 3;" >
                          <div class="wizard-header1">
                          <div class="signuphead" style=" float: right; 
                          width: 25%;
                          background: #2e3442;
                          color: #fff;
                          text-align: center;
                          padding: 5px 0%;
                          border: 1px solid #2e3442;
                          border-radius: 10px 10px 0px 0px;
                          font-size: 13px;
                          font-weight: bolder;">Forgot Password</div>
                       </div>
                             <div class="signupbody" style="float: left; width: 100%; border-top: 5px solid #2e3442; border-radius: 10px 0px 0px 0px;">
                                <div class="card wizard-card" data-color="green" style=" min-height: 350px;
                                box-shadow: 0 20px 16px -15px rgba(0, 0, 0, 0.57);  border-radius: 6px;
                                box-shadow: 0 2px 2px rgba(204, 197, 185, 0.5);
                                background-color: #fafafa;
                                color: #252422;
                                padding: 10px 0;
                                margin-bottom: 20px;
                                position: relative;
                                z-index: 1;" >
                                   <form action="">
                                      <div class="row" style="margin-left:-15px;margin-right:-15px">
                                         <div class="col-sm-10 col-sm-offset-1"style="width:83.33333333%;margin-left:8.33333333%">
                                            <div class="col-sm-12" style="float:left; width:100%">
                                               <div class="wizard-header1">
                                                  <div class="usericon" style="width: 15%;
                                                  margin: auto;"><img alt="verify" src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/verify.png"></div>
                                               </div>
                                               <div class="row" style="margin-left:-15px;margin-right:-15px">
                                                  <div class="col-sm-12" style="float:left; width:100%">
                                                     <div class="verifyinfo" style=" float: left;
                                                     width: 100%;
                                                     text-align: center;
                                                     padding: 20px 0%;">Thanks for choosing our service</div>
                                                  </div>
                                               </div>
                                               <div class="row" style="margin-left:-15px;margin-right:-15px">
                                                  <div class="col-sm-12" style="float:left; width:100%">
                                                     <div class="verifyinfo" style=" float: left;
                                                     width: 100%;
                                                     text-align: center;
                                                     padding: 20px 0%;">We are here to help you</div>
                                                  </div>
                                               </div>
                                               <div class="row" style="margin-left:-15px;margin-right:-15px">
                                                  <div class="col-sm-12" style="float:left; width:100%">
                                                     <div class="verifyinfo" style=" float: left;
                                                     width: 100%;
                                                     text-align: center;
                                                     padding: 20px 0%;">Click below button to change password</div>
                                               </div>
                                               <div class="row" style="margin-left:-15px;margin-right:-15px">
                                                  <a href="${rootUrl}/api/users/update?email=${userMail}&code=${temp}"><input type="submit" name="submit" class="submitlink" style="float: left;
                                                  background-color: #2e3442;
                                                  border: #2e3442;
                                                  border-radius: 5px;
                                                  width: 50%;
                                                  text-align: center;
                                                  padding: 8px 0%;
                                                  color: #FFFFFF;
                                                  font-weight: bold;
                                                  margin: 30px 25%;" value="Change Password"></a>
                                               </div>
                                            </div>
                                         </div>
                                      </div>
                                   </form>
                                </div>
                             </div>
                          </div>
                          <!-- wizard container -->
                       </div>
                    </div>
                    <!-- row -->
                 </div>
                 <!--  big container -->
              </div>
           </body>
           <!--   Core JS Files   -->
           <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>   
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
           <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
           <!--  Plugin for the Wizard -->
           <script src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/js/paper-bootstrap-wizard.js" type="text/javascript"></script>   
           <script src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/js/jquery.validate.min.js" type="text/javascript"></script>
           <script>
              $(document).ready(function(){         
                  //$("#basketone").show();
                  $("#baskettwo").show();
              
                  //$("#planone").show();
                  $("#plantwo").show(); 
              
                  //$("#paymentone").show();
                  $("#paymenttwo").show();  
              });
           </script>    
        </html>`
	};
};
const welcomeMail = (userMail,storeName,pwd,rootUrl) => {
	var admin_url="https://admin.cadenza.tech";
	var public_url="https://"+storeName+".cadenza.tech/admin";
	return {
		from: `Welcome mail for Store owner <${fromMail}>`,
		to: `${userMail}`,
		subject: 'Welcome mail',
		html: `<html lang="en">
        <head>
           <meta charset="utf-8" />
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
           <link rel="apple-touch-icon" sizes="76x76" href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/apple-icon.png" />
           <link rel="icon" type="image/png" href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/favicon.png" />
           <title>Welcome</title>
           <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
           <meta name="viewport" content="width=device-width" />
           <!-- CSS Files -->
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
           <link href="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/css/styles.css" rel="stylesheet" />
           <!-- Fonts and Icons -->
           <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
          
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu" />
        
        </head>
        <body style="color: #66615b;
        font-size: 24px;
        font-family: "Ubuntu">
           <div class="image-container set-full-height" style=" min-height: 100vh;
           background-position: center center;
           background-size: cover;
           background-image: url('https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/bg.png');">
              <div class="header" style="float:left; width: 100%;">
                 <div class="logosection" style="width: 14%;margin: 40px 36% 0px;">         
                    <a href="#"><img src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/img/new_logo.png"></a>
                 </div>
              </div>
              <!--   Big container   -->
              <div class="container" style="margin-right:auto;margin-left:auto;padding-left:15px;padding-right:15px;box-sizing: border-box;">
                 <div class="row" style="margin-left:-15px;margin-right:-15px">
                    <div class="col-sm-4 col-sm-offset-4" style="margin-left:33.33333333%;width:33.33333333%;">
                       <!--      Wizard container        -->
                       <div class="wizard-container" style="z-index: 3;" >
                          <div class="wizard-header1">
                             <div class="signuphead" style=" float: right; 
                             width: 25%;
                             background: #2e3442;
                             color: #fff;
                             text-align: center;
                             padding: 5px 0%;
                             border: 1px solid #2e3442;
                             border-radius: 10px 10px 0px 0px;
                             font-size: 13px;
                             font-weight: bolder;">Welcome</div>
                          </div>
                          <div class="signupbody" style="float: left; width: 100%; border-top: 5px solid #2e3442; border-radius: 10px 0px 0px 0px;">
                             <div class="card wizard-card" data-color="green" style=" min-height: 350px;
                             box-shadow: 0 20px 16px -15px rgba(0, 0, 0, 0.57);  border-radius: 6px;
                             box-shadow: 0 2px 2px rgba(204, 197, 185, 0.5);
                             background-color: #fafafa;
                             color: #252422;
                             padding: 10px 0;
                             margin-bottom: 20px;
                             position: relative;
                             z-index: 1;" >
                                <form action="" method="">
                                   <div class="row" style="margin-left:-15px;margin-right:-15px">
                                      <div class="col-sm-10 col-sm-offset-1" style="width:83.33333333%;margin-left:8.33333333%">
                                         <div class="col-sm-12" style="float:left; width:100%">
                                            <div class="wizard-header1">
                                               <h4  class="signupcz" style="text-align:center;font-weight:600; margin:15px 0px;">Welcome to Cadenza</h4>
                                               <h4  class="signupcz" style="margin: 20px 0px;font-size: 12px;">Your login information</h4>
                                            </div>
                                            <div class="row" style="margin-left:-15px;margin-right:-15px">
                                               <div class="col-sm-12" style="float:left; width:100%">
                                                  <div class="wellabel" style="float: left; width: 37%; padding: 10px 0%;"><label>Store admin URL</label></div>
                                                  <div class="welinpu" style="float: left; width: 63%;" ><input type="text" class="form-control" placeholder="" readonly="readonly" value="${admin_url}"></div>
                                               </div>
                                            </div>
                                            <div class="row" style="margin-left:-15px;margin-right:-15px" >
                                               <div class="col-sm-12" style="float:left; width:100%">
                                                  <div class="wellabel" style="float: left; width: 37%; padding: 10px 0%;"><label>Store Password</label></div>
                                                  <div class="welinpu" style="float: left; width: 63%;"><input type="text" class="form-control" placeholder="" readonly="readonly" value="${pwd}"></div>
                                               </div>
                                            </div>
                                            <div class="row" style="margin-left:-15px;margin-right:-15px" >
                                               <div class="col-sm-12" style="float:left; width:100%">
                                                  <div class="wellabel" style="float: left; width: 37%; padding: 10px 0%;"><label>Store Puvlic URL</label></div>
                                                  <div class="welinpu" style="float: left; width: 63%;"><input type="text" class="form-control" placeholder="" readonly="readonly" value="${public_url}"></div>
                                               </div>
                                            </div>    
     
                                            <div class="row" style="margin-left:-15px;margin-right:-15px">
                                               <div class="col-sm-12" style="float:left; width:100%">
                                                  
                                                  <div class="confirmwelcome" style="float: left; width: 100%; margin: 20px 0%;">
                                                     <input type="checkbox" name="">
                                                     <span>I agree to the Terms & Conditions.</span>
                                                  </div>
                                                  
                                               </div>
                                            </div>    
     
                                            <div class="row" style="margin-left:-15px;margin-right:-15px">
                                               <input type="submit" name="submit" class="submitlink" style="float: left;
                                               background-color: #2e3442;
                                               border: #2e3442;
                                               border-radius: 5px;
                                               width: 50%;
                                               text-align: center;
                                               padding: 8px 0%;
                                               color: #FFFFFF;
                                               font-weight: normal;
                                               margin: 30px 25%;" value="Continue to Login">
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                </form>
                             </div>
                          </div>
                       </div>
                       <!-- wizard container -->
                    </div>
                 </div>
                 <!-- row -->
              </div>
              <!--  big container -->
           </div>
        </body>
        <!--   Core JS Files   -->
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
        <!--  Plugin for the Wizard -->
        <script src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/js/paper-bootstrap-wizard.js" type="text/javascript"></script>  
        <script src="https://s3.ap-south-1.amazonaws.com/cadenza-assets/assets1/js/jquery.validate.min.js" type="text/javascript"></script>
        <script>
           $(document).ready(function(){            
               //$("#basketone").show();
               $("#baskettwo").show();
           
               //$("#planone").show();
               $("#plantwo").show();    
           
               //$("#paymentone").show();
               $("#paymenttwo").show(); 
           });
        </script>   
     </html>`
	};
};

module.exports = {
	sendVerificationMail,
	forgotPasswordMail,
	welcomeMail
}