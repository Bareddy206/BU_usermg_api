const router = require('express').Router();
const rand = require('csprng');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const randomstring = require('randomstring');

const config = require('../../config/config');
const User = require('../../models/user');
const Storename = require('../../models/storename');
const Session = require('../../models/session');


const encrypt = require('../../methods/encrypt');
const sendMail = require('../../methods/email');
const statusMsg = require('../../methods/statusMsg');

var tableName = require('../../config/table');

const crypto=require('crypto');
const cryptoJs= require('crypto-js');

// console.log(tableName);
const mailgun = require('mailgun-js')({
	apiKey: config.API_KEY_MAILGUN,
	domain: config.MAILGUN_DOMAIN
});

router.get('/', (req, res) => {
	res.send('This is the api home route for User management');
})

//Login
router.post('/login', (req, res) => {
	const bodyParams = req.body;
	var first_time;
	//console.log('bodyParams: ', bodyParams);
	const userEmail = bodyParams.email;

	User.selectTable('Merchant_details');
	User.getItem(userEmail, {})
	.then((user) => {
		if (Object.keys(user).length === 0) {
			res.send({status:'user not found'});
		}else {
			return user;
		}
	}).then((user) => {
		const hashPassword = user.password;
		bcrypt.compare(bodyParams.password, hashPassword, (err, result) => {
			if (err) {
				res.send(statusMsg.errorResponse(err));
			}
			if (result) {

				if(user.verifiedornot==='yes')
				{
					var tableName=user.storeName+"_session_data";
					console.log('tableName::'+tableName);
					Session.selectTable(tableName);
					// 	if(user.first_time==='yes')
					// {
					// 	first_time='yes';
					// 	const updateParams = {
					// 		email: userEmail,
					// 		first_time: 'no'
					// 	}
					// 	User.updateItem(updateParams,{},(err,data)=>{
					// 		if(err)
					// 		{
					// 			res.send(statusMsg.errorResponse(err));
					// 			console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
					// 		}
					// 		else{
					// 			console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
					// 		}

					// 	});
					// }	
					// else
					// 	first_time='no';
					var date = new Date();

					// add a day
					date.setDate(date.getDate() + 1);
					console.log(result);
					const random = randomstring.generate();
					console.log('randomstring::'+random);
					var putParams={
						session_id:random,
						product_ids:'[123]',
						subscription_ids:'[123]'
					}
					console.log("putParams::"+JSON.stringify(putParams));
					Session.createItem(putParams,{table:tableName, overwrite: false })
					.then((session) => {
						token= jwt.sign({
							email: `${userEmail}`,
							storeName: `${user.storeName}`,
							sessionId:`${random}`,
							expiresIn: '24h'
						}, config.SECRET_KEY);
						res.send({ 
							"status":'Successfully logged in!', 
							"token": token , 
							"storeName":`${user.storeName}`,
							"first_time":'yes'
						});
						
					}).catch((err) => {
						console.log("Error while creating in session");
						res.send({status:"failure",message:"session creation error"});
					})
				}
				else
				{
					res.send({
						status:"failure",
						message:"please verify your email!"
					});
					
				}
				
			} else {
				res.send({status:"incorrect password" , message:"incorrect password"});
			}
		})
	}).catch((err) => {
		res.send({status:err});
	})
});

//Signup
router.post('/signup', (req, res) => {
	const bodyParams = req.body;
	//console.log('bodyParams: ', bodyParams);
	const name = bodyParams.storeName;
	const userEmail=bodyParams.email;
	const rootUrl = `${req.protocol}://${req.get('host')}`; //equates to http :// localhost or ec2hostname

	if (bodyParams.password !== bodyParams.cpassword) {
		res.send({status:"failure", message:"Password doesn't match"});
	}
	//select table before
	 Storename.selectTable('store_details');
	 Storename.getItem(name, {})
	 .then((name) => {
		if (Object.keys(name).length > 0) {
			res.send({
				status: 'failure',
				message: 'Storename already taken'
			})
		}else{
			return name;
		}
	 }).then((name) => {
		//console.log('storename',name);
		User.selectTable('Merchant_details');
		User.getItem(userEmail, {})
		.then((user) => {
			if (Object.keys(user).length > 0) {
				res.send({
					status: 'failure',
					message: 'emailId already taken'
				})
			}else {
				return user;
			}
		}).then((user) => {
				const createCallback = (hashnewpwd) => {
					const verification_code = randomstring.generate();
					const putParams = {
						"email": userEmail,
						"username": bodyParams.username,
						"storeName": bodyParams.storeName,
						"password": hashnewpwd,
						"verification_code": verification_code,
						"verifiedornot": 'no',
						"first_time":'yes',
					};
					console.log("\nAdding a new item...");
					User.createItem(putParams, {table:'Merchant_details', overwrite: false })
					.then((user) => {
						console.log('\nAdded user');
						const params = {"storeName": bodyParams.storeName };
						Storename.createItem(params, {table:'store_details', overwrite: false })
						.then((name) => {
							console.log('\nAdded storename');
							const mailData = sendMail.sendVerificationMail(userEmail,bodyParams.storeName, verification_code,rootUrl,bodyParams.password);
							mailgun.messages().send(mailData, (err, body) => {
								if (err) {
									res.send(statusMsg.errorResponse(err));
									//console.log('err: ', err);
								} else {
									res.send({
										status: "success",
										message: "email sent to your mailid"
									});
									//res.redirect('/signin');
								}
							});
						}).catch((err) => {
							res.send(statusMsg.errorResponse(err));
						})
								
					}).catch((err) => {
						res.send(statusMsg.errorResponse(err));
					})
				}
				encrypt.generateSalt(res, bodyParams.password, createCallback);
				
		}).catch((err) => {
			res.send(statusMsg.errorResponse(err))
		})		
	 }).catch((err) => {
		res.send(statusMsg.errorResponse(err))
	 })
		
});

//storevalidation
router.post('/storecheck',(req,res)=>{
	var storeName=req.body.storeName;
	//console.log(storeName);

	Storename.selectTable('store_details');
	Storename.getItem(storeName,{})
	.then((sname) => {
		if(Object.keys(sname).length==0)
		{
			res.send({
				status: 'success',
				message: 'proceed'
			});
		}else{
			return sname;
		}
	}).then((sname) => {
		res.send({
			status: 'failure',
			message: 'storeName already exist'
		});
	}).catch((err) => {
		res.send(statusMsg.errorResponse(err));
	})
});

//email verification
router.post('/emailcheck',(req,res)=>{
	var email=req.body.email;
	//console.log("emailcheck:" + email);

	User.selectTable('Merchant_details');
	User.getItem(email,{})
	.then((user) => {
		if (Object.keys(user).length === 0) {
			res.send({
				status:"OK",
				message:"you can procced"
			});
		}else {
			return user;
		}
	}).then((user) => {
		res.send({
			status:"failure",
			message:"email already taken"
		});
	}).catch((err) => {
		res.send(statusMsg.errorResponse(err));
	})
});

//Signup Verification
router.get('/signupverification', (req, res) => {

	const queryParams = req.query;
	//console.log(queryParams);
	const userEmail = queryParams.email;
	const rootUrl = `${req.protocol}://${req.get('host')}`;

	User.selectTable('Merchant_details');
	User.getItem(userEmail, {})
	.then((user) => {
		if (Object.keys(user).length === 0) {
			res.send({
				status: 'failure',
				message: 'EmailId and token do not match'
			})
		}else {
			return user;
		}
	}).then((user) => {
		if (Object.keys(user).length > 0) {
			//console.log(user.verification_code,queryParams.verification_code);
			if(user.verifiedornot==='yes')
			{
				console.log("already verified:");
				res.redirect('http://localhost:9000');
			}
			else if (user.verification_code === queryParams.verification_code) {
				const updateParams = {
					email: userEmail,
					verifiedornot: 'yes'
				}
				console.log("Updating the item...");
				User.updateItem(updateParams, {})
				.then((data) => {
					//console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));

					var res1 = tableName.tempcreateTables(req.query.storeName, {}, (err, table) => {
						if (err) {
							console.log(err);
						} else {
							console.table("this is " + table);
							console.log("Table create result:"+ table);
						}
					});
					const mailData = sendMail.welcomeMail(userEmail,req.query.storeName,req.query.pwd,rootUrl);
					mailgun.messages().send(mailData, (err, body) => {
						if (err) {
							res.send(statusMsg.errorResponse(err));
							console.log('err: ', err);
						} else {
							console.log("welcome mail is sent");
						}
					});
					res.redirect('http://localhost:9000/anim');

				}).catch((err) => {
					res.send(statusMsg.errorResponse(err));
					console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
				})
					
			}else {
				res.send({
					status: 'failure',
					message: 'auth code is not correct'
				});
			}
		}

	}).catch((err) => {
		console.log('\nerr', err);
		res.send(statusMsg.errorResponse(err));
	})
});

// Forgot Password
router.post('/forgotpwd', (req, res) => {
	const bodyParams=req.body;
	const userEmail = req.body.email;
	const rootUrl = `${req.protocol}://${req.get('host')}`; //equates to http :// localhost or ec2hostname

	User.selectTable('Merchant_details');
	User.getItem(userEmail,{})
	.then((user) => {
		if (Object.keys(user).length === 0) {
			res.send({
				status: 'failure',
				message: 'User not Exist'
			})
		}else {
			return user;
		}
	}).then((user) => {
		if(Object.keys(user).length > 0){
			const temp =rand(24, 24);
			const updateParams = {
				email: userEmail,
				temp_str: temp
			};
			console.log("Updating the item...");
			User.updateItem(updateParams, {})
			.then((data) => {
				const dataToSend = sendMail.forgotPasswordMail(userEmail,rootUrl,temp);
			    mailgun.messages().send(dataToSend, (error, body) => {
					if (error) {
						res.send(statusMsg.errorResponse(err));
					} else {
						console.log("email sent");
						//console.log('\nbody\n',body);
						res.send({
							status: "success",
							message: "email sent to your mailid"
						});
					}
			    })
			}).catch((err) => {
				res.send(statusMsg.errorResponse(err));
				//console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
			})
	    }
	
	}).catch((err) => {
		console.log('\nerr', err);
		res.send(statusMsg.errorResponse(err));
	})
});
//change password
router.get('/chngpwd',(req,res)=>{
	const bodyParams=req.body;
	const userEmail=bodyParams.email;
	const code=bodyParams.code;
	const newpwd=bodyParams.newpassword;

	User.selectTable('Merchant_details');
	User.getItem({ email: userEmail }, {})
	.then((user) => {
		if (Object.keys(user).length === 0) {
			res.send(statusMsg.incorrectResponse('user'));
		}else {
			return user;
		}
	}).then((user) => {
		if(Object.keys(user).length > 0) {
			if(user.temp_str===code) {
			    const updateCallback = function (hashnewpwd) {
					const updateParams = {
						email: userEmail,
						password: hashnewpwd
					};
					console.log("Updating the item...");
					User.updateItem(updateParams, {})
					.then((data) => {
						res.send({
							status: 'success',
							message: 'changed password successfully'
						})
					}).catch((err) => {
						res.send(statusMsg.errorResponse(err));
						//console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
					})
			    };
			    encrypt.generateSalt(res, bodyParams.newpassword, updateCallback);
		    }
		}else {
			return user;
		}
	}).then((user) => {
		res.send({
			status: 'failure',
			message: 'code is not correct'
		});
	}).catch((err) => {
		res.send(statusMsg.errorResponse(err));
	})
});

router.get('/resetpassword',(req,res)=>{
	var email=req.query.email;
	var code=req.query.code;
	//res.render('admin2/change_password',{email:email});
	res.redirect("http://localhost:9000/changepwd?email="+email);

});

// Update Password
router.post('/updatepwd', (req, res) => {
	const bodyParams = req.body;
	const userEmail = bodyParams.email;

	User.selectTable('Merchant_details');
	User.getItem({ email: userEmail }, {})
	.then((user) => {
		if (Object.keys(user).length === 0) {
			res.send(statusMsg.incorrectResponse('user'));
		}else {
			return user;
		}
	}).then((user) => {
		const updateCallback = function (hashnewpwd) {
			const updateParams = {
				email: userEmail,
				password: hashnewpwd
			};
			console.log("Updating the item...");
			User.updateItem(updateParams, {})
			.then((data) => {
				res.send({
					status: 'success',
					message: 'updated successfully'
				})
			}).catch((err) => {
				res.send(statusMsg.errorResponse(err));
			})
		};
		encrypt.generateSalt(res, bodyParams.newpassword, updateCallback);
	}).catch((err) => {
		res.send(statusMsg.errorResponse(err));
	})
});

// Reset password
router.post('/resetpwd', (req, res) => {
	const bodyParams = req.body;
	const userEmail = bodyParams.email;

	User.getItem({ email: userEmail }, {})
	.then((user) => {
		if (Object.keys(user).length === 0) {
			res.send(statusMsg.incorrectResponse('user'));
		}else {
			return user;
		}
	}).then((user) => {
		const hashPassword = user.password;
		bcrypt.compare(bodyParams.password, hashPassword, (err, result) => {
			if (err) {
				console.log('\nerr: ', err);
				res.send(statusMsg.errorResponse(err));
			} else if (result) {
				// var token_info = {token: jwt.sign({ email: 'harishkashaboina94@gmail.com', exp: Math.floor(Date.now() / 1000) + (60 * 60)}, 'cadenza')};
				const updateCallback = (hashnewpwd) => {
					const updateParams = {
						email: userEmail,
						password: hashnewpwd
					};
					User.updateItem(updateParams, {})
					.then((data) => {
						res.send(statusMsg.verifySuccess);
					}).catch((err) => {
						res.send(statusMsg.errorResponse(err));
					})
				}
				encrypt.generateSalt(res, bodyParams.newpassword, updateCallback);
			} else {
				res.send(statusMsg.incorrectResponse('password'));
			}
		})
	}).catch((err) => {
		res.send(statusMsg.errorResponse(err));
	})
});



router.post('/add_merchant_account_details',(req,res)=>{
	var token=req.body.token;
	if(token)
	{
		jwt.verify(token,config.SECRET_KEY,(err,decode)=>{

			if(err)
			{
				res.send({status:"failure",message:"Invalid Token"});
			}else {

				var MID=req.body.MID;
				//console.log("suma Testing:"+ MID);
				var MERCHANT_KEY=req.body.MERCHANT_KEY;
				const secret = '#phoenix@haroz@1465';

				// Encrypt 
				const encrypted_mid = cryptoJs.AES.encrypt(MID, secret);
				const encrypted_merchant_key=cryptoJs.AES.encrypt(MERCHANT_KEY, secret);
				//console.log('encryped data:\n'+encrypted_mid+'\n'+encrypted_merchant_key);

				// Decrypt 
				var bytes  = cryptoJs.AES.decrypt(encrypted_mid.toString(), secret);
				var plaintext = bytes.toString(cryptoJs.enc.Utf8);
				var mid=encrypted_mid.toString();
				var merchant_key=encrypted_merchant_key.toString();
				//console.log("decrypted data:"+ plaintext);
				const store=decode.storeName;
				//console.log("storeName:"+ store);
	
				const putParams={
					storeName:store,
					MID:mid,
					MERCHANT_KEY:merchant_key
				};
				//console.log("putParams:"+ putParams.MID);

				Storename.selectTable('store_details');
				Storename.updateItem(putParams,{})
				.then((item) => {
					console.log("UpdateItem succeeded:");
					res.send({
						status: 'success',
						message: 'Payment configuration is done'
					});
				}).catch((err) => {
					res.send({status:"failure",message:"update item error"});
				})
			}
		})

	}
	else{
		res.send({status:"failure",message:"please send a token!"});
	}
	
});


module.exports = router;